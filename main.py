import nextcord, requests
from config import token, ids

import logging
logging.basicConfig(level=logging.INFO)

intents = nextcord.Intents.default()
intents.members = True
bot = nextcord.Client(intents=intents)

@bot.slash_command(description="Some description goes here", guild_ids=ids)
async def ud(interaction, word:str = nextcord.SlashOption(name='word', description='The word you want to define', required=True)):
    r = requests.get(f"https://api.urbandictionary.com/v0/define?term={word}")
    d = r.json()['list'][0]
    embed = nextcord.Embed(title=word, url=d['permalink'])
    embed.add_field(name='Definition', value=d['definition'], inline=False)
    embed.add_field(name='Example', value=d['example'], inline=False)
    embed.add_field(name='Score', value=f"👍 {d['thumbs_up']} 👎 {d['thumbs_down']}", inline=False)
    await interaction.response.send_message(ephemeral=True, embed=embed)

bot.run(token)
