# 🎨 ud
![screenshot](Put a screenshot here)

Simple urban dictionary bot

## Requirements
* Python3
* Linux system with systemd
* Discord bot (with intents for messages enabled)

## Installation

Clone the repository, go in it, install nextcord, copy the template config file, edit it and run the script.

```bash
git clone https://codeberg.org/botaddicts/ud
cd ud/
pip install -r requirements.txt
cp config.template.py config.py
nano config.py
python3 main.py
```

And for systemd: (running in the background):

```bash
sed -i "s|ABSOLUTE_PATH|$(pwd)|g" ud.service
sed -i "s|USER|$USER|g" ud.service
sudo cp ud.service /etc/systemd/system/
sudo systemctl start ud
```

## Usage
Use `/ud` followed by the word you want to define. 
